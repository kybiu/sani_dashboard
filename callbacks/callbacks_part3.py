from dash.dependencies import Input, Output
import plotly.graph_objects as go
import pandas as pd
from callbacks.utility import connect_db
import plotly.express as px
from sqlalchemy import sql


def get_callbacks_part3(app, conn):
    @app.callback(
        [
            Output(component_id="conv_by_day", component_property="figure"),
            Output(component_id="silent_rate_table", component_property="figure")
        ],
        Input('date_picker', 'start_date'),
        Input('date_picker', 'end_date')
    )
    def update_conv_by_day(start_date, end_date):
        df_conv_by_day = pd.read_sql(sql.text("""
            select  f.date, count( distinct f.user_id) as "Số lượng user", sum(f.no_conv) as "Số lượng conversation", sum(f.no_silent) as "Số lượng conversation Sani không trả lời"
            from fact f
            where f.date between \'{}\' and \'{}\'
            group by f.date
            order by f.date asc;
            """.format(start_date, end_date)), conn)

        colors = {
            "Số lượng user": "#5E239D",
            "Số lượng conversation": "#00F0B5",
            "Số lượng conversation Sani không trả lời": "#F61067"
        }
        # colors = {
        #     "Số lượng user": "#F3DE8A",
        #     "Số lượng conversation": "#CAE7B9",
        #     "Số lượng conversation Sani không trả lời": "#EB9486"
        # }
        df_conv_by_day["silent_rate"] = ((df_conv_by_day["Số lượng conversation Sani không trả lời"] * 100) / df_conv_by_day["Số lượng conversation"]).round(2)
        df_conv_by_day["silent_rate"] = df_conv_by_day["silent_rate"].apply(lambda x: str(x) + " %")
        try:
            fig = px.bar(df_conv_by_day,
                         x="date", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
                         title="Số conversation theo ngày", text_auto=True, barmode="group", color_discrete_map=colors)
            df_conv_by_day["date"] = df_conv_by_day["date"].apply(lambda x: str(x.day) + "/" + str(x.month))
            silent_rate_table = go.Figure(data=[go.Table(header=dict(values=list(df_conv_by_day["date"])),
                                                         cells=dict(values=df_conv_by_day[["Số lượng conversation", "Số lượng conversation Sani không trả lời", "silent_rate"]].to_numpy().tolist()))
                                                ])
        except:
            fig = px.bar(df_conv_by_day,
                         x="date", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
                         title="Số conversation theo ngày", text_auto=True, barmode="group", color_discrete_map=colors)
            df_conv_by_day["date"] = df_conv_by_day["date"].apply(lambda x: str(x.day) + "/" + str(x.month))
            silent_rate_table = go.Figure(data=[go.Table(header=dict(values=list(df_conv_by_day["date"])),
                                                         cells=dict(values=df_conv_by_day[["Số lượng conversation", "Số lượng conversation Sani không trả lời", "silent_rate"]].to_numpy().tolist()))
                                                ])
        fig.update_layout(legend_title="")
        return fig, silent_rate_table
