from dash.dependencies import Input, Output
import plotly.graph_objects as go
import pandas as pd
from datetime import datetime, timedelta
from callbacks.utility import connect_db
from sqlalchemy import sql

today = datetime.today().strftime('%Y-%m-%d')
ten_day_before_today = (datetime.today() - timedelta(days=9)).strftime('%Y-%m-%d')
yesterday = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')
ten_day_before_yesterday = (datetime.today() - timedelta(days=10)).strftime('%Y-%m-%d')
month_year_before_last_month = ((datetime.today().replace(day=1) - timedelta(days=1)).replace(day=1) - timedelta(days=1)).strftime('%m-%Y')


graph_width = 350
indicator_font_sz = 20


def get_callbacks_part1(app, conn):
    @app.callback(
        Output(component_id="resp_rate", component_property="figure"),
        Input(component_id="graph-update", component_property="n_intervals")
    )
    def update_resp_rate(n_intervals: int):

        df_today = pd.read_sql(sql.text("""
            select ROUND(sum(no_resp)*100/sum(no_conv), 2) as resp_rate,
                   ROUND(sum(no_silent)*100/sum(no_conv), 2) as silent_rate
            from fact
            where date between \'{}\' and \'{}\'
            """.format(ten_day_before_today, today)), conn)

        df_yesterday = pd.read_sql(sql.text("""
            select ROUND(sum(no_resp)*100/sum(no_conv), 2) as resp_rate,
                   ROUND(sum(no_silent)*100/sum(no_conv), 2) as silent_rate
            from fact
            where date between \'{}\' and \'{}\'
            """.format(ten_day_before_yesterday, yesterday)), conn)

        if df_today["resp_rate"].values[0] >= 80:
            colors = ['#86BA90', '#EEEEFF']
        elif df_today["resp_rate"].values[0] >= 40:
            colors = ['#F3DE8A', '#D8DBE2']
        else:
            colors = ['#EB9486', '#D8DBE2']

        fig = go.Figure(data=[go.Pie(labels=list(df_today.columns), values=list(df_today.values[0]), hole=.7)])
        fig.update_traces(hoverinfo='none', textinfo='none', rotation=180, marker=dict(colors=colors))
        fig.update_layout(showlegend=False,
                          width=graph_width,
                          title={
                              'text': "Response rate",
                              'y': 0.75,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'
                          })

        fig.add_trace(go.Indicator(
            mode="number+delta",
            value=df_today["resp_rate"].values[0],
            delta={'reference': df_yesterday["resp_rate"].values[0]},
            number={'suffix': "%", "font": {"size": indicator_font_sz}},
            domain={'row': 0, 'column': 1}))

        return fig

    @app.callback(
        Output(component_id="user_retention_rate", component_property="figure"),
        Input(component_id="graph-update", component_property="n_intervals")
    )
    def update_retention_rate(n_intervals: int):
        last_month_year = (datetime.today().replace(day=1) - timedelta(days=1)).strftime('%m/%Y')
        if last_month_year[0] == "0":
            last_month_year = last_month_year[1:]
        all_user = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from fact
            """), conn)

        all_user_last_month = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from dim_user
            where month_start like '{}'
            or month_return like '%%{}%%'
            """.format(last_month_year, last_month_year)), conn)

        all_old_user_last_month = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from dim_user
            where month_return like \'%%{}%%\'
            """.format(last_month_year)), conn)
        retention_rate = all_old_user_last_month.values[0][0] * 100 / (all_user.values[0][0] - all_user_last_month.values[0][0])

        fig = go.Figure(data=[go.Pie(labels=["a", "b"], values=[retention_rate, 100 - retention_rate], hole=.7)])
        fig.update_traces(hoverinfo='none', textinfo='none', rotation=180, marker=dict(colors=['#86BA90', '#EEEEFF']))
        fig.update_layout(showlegend=False,
                          width=graph_width,
                          title={
                              'text': "Retention rate",
                              'y': 0.75,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'
                          })

        fig.add_trace(go.Indicator(
            mode="number+delta",
            value=retention_rate,
            # delta={'reference': df_yesterday["resp_rate"].values[0]},
            number={'suffix': "%", "font": {"size": indicator_font_sz}},
            domain={'row': 0, 'column': 1}))

        return fig

    @app.callback(
        Output(component_id="success_rate", component_property="figure"),
        Input(component_id="graph-update", component_property="n_intervals")
    )
    def update_success_rate(n_intervals: int):
        last_month_year = (datetime.today().replace(day=1) - timedelta(days=1)).strftime('%m/%Y')
        all_user = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from fact
            """), conn)

        all_user_last_month = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from dim_user
            where month_start like '{}'
            or month_return like '%%{}%%'
            """.format(last_month_year, last_month_year)), conn)

        all_old_user_last_month = pd.read_sql(sql.text("""
            select count(distinct user_id)
            from dim_user
            where month_return like \'%%{}%%\'
            """.format(last_month_year)), conn)
        retention_rate = all_old_user_last_month.values[0][0] * 100 / (all_user.values[0][0] - all_user_last_month.values[0][0])

        fig = go.Figure(data=[go.Pie(labels=["a", "b"], values=[retention_rate, 100 - retention_rate], hole=.7)])
        fig.update_traces(hoverinfo='none', textinfo='none', rotation=180, marker=dict(colors=['#86BA90', '#EEEEFF']))
        fig.update_layout(showlegend=False,
                          width=graph_width,
                          title={
                              'text': "Success rate",
                              'y': 0.75,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'
                          })

        fig.add_trace(go.Indicator(
            mode="number+delta",
            value=retention_rate,
            # delta={'reference': df_yesterday["resp_rate"].values[0]},
            number={'suffix': "%", "font": {"size": indicator_font_sz}},
            domain={'row': 0, 'column': 1}))

        return fig
