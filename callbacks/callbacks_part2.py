from dash.dependencies import Input, Output
import plotly.graph_objects as go
import pandas as pd
from callbacks.utility import connect_db
import plotly.express as px
from sqlalchemy import sql


def get_callbacks_part2(app, conn):
    @app.callback(
        Output(component_id="conv_by_month", component_property="figure"),
        Input(component_id="graph-update", component_property="n_intervals")
    )
    def update_conv_by_month(n_intervals: int):
        df_conv_by_month = pd.read_sql(sql.text("""
                select  dd.month, count( distinct f.user_id) as "Số lượng user", sum(f.no_conv) as "Số lượng conversation", sum(f.no_silent) as "Số lượng conversation Sani không trả lời"
                from fact f join dim_date dd
                on f.date = dd.full_date
                group by dd.month
                order by dd.month asc;
            """), conn)

        colors = {
            "Số lượng user": "#F3DE8A",
            "Số lượng conversation": "#CAE7B9",
            "Số lượng conversation Sani không trả lời": "#EB9486"
        }
        try:
            fig = px.bar(df_conv_by_month,
                         x="month", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
                         title="Số conversation theo tháng", text_auto=True, barmode="group", color_discrete_map=colors)
        except:
            fig = px.bar(df_conv_by_month,
                         x="month", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
                         title="Số conversation theo tháng", text_auto=True, barmode="group", color_discrete_map=colors)
        fig.update_layout(
            legend_title="",
            legend=dict(
                orientation="h",
                yanchor="top",
                # xanchor="right",
                y=-0.2,
                # x=1,
            )
        )
        return fig

    @app.callback(
        Output(component_id="user_back_by_month", component_property="figure"),
        Input(component_id="graph-update", component_property="n_intervals")
    )
    def update_user_back_by_month(n_intervals: int):
        df_user_by_month = pd.read_sql(sql.text("""
                select  dd.month, count( distinct f.user_id) as "Tổng số user"
                from fact f join dim_date dd
                on f.date = dd.full_date
                group by dd.month
                order by dd.month asc;
            """), conn)

        df_new_user_by_month = pd.read_sql(sql.text("""
                select month_start, count(distinct user_id) as "Số user mới"
                from dim_user
                group by  month_start
            """), conn)

        df_new_user_by_month["month"] = df_new_user_by_month["month_start"].apply(lambda x: int(x[:-5]))
        df_new_user_by_month = df_new_user_by_month.drop('month_start', 1)
        df_user = pd.merge(df_user_by_month, df_new_user_by_month, on='month')
        df_user["Số user quay lại với Sani"] = df_user["Tổng số user"] - df_user["Số user mới"]

        colors = {
            "Số lượng user": "#F3DE8A",
            "Số lượng conversation": "#CAE7B9",
            "Số lượng conversation Sani không trả lời": "#EB9486"
        }

        fig = go.Figure(data=[
            go.Bar(name='Số user quay lại nhắn với Sani', x=df_user["month"], y=df_user["Số user quay lại với Sani"],
                   text=df_user["Số user quay lại với Sani"],
                   textposition='auto', marker_color='#F3DE8A'),

            go.Bar(name='Tổng số user', x=df_user["month"], y=df_user["Tổng số user"],
                   text=df_user["Tổng số user"],
                   textposition='auto', marker_color='#CAE7B9')
        ])
        fig.update_layout(barmode='group')
        fig.update_layout(
            title="Tỷ lệ khách quay lại với Sani",
            xaxis_title="Tháng",
            yaxis_title="Số lượng user",
        )

        fig.update_layout(
            legend_title="",
            legend=dict(
                orientation="h",
                yanchor="top",
                # xanchor="right",
                y=-0.2,
                # x=1,
            ))
        return fig

    # @app.callback(
    #     Output(component_id="place_and_purpose", component_property="figure"),
    #     Input(component_id="graph-update", component_property="n_intervals")
    # )
    # def update_place_and_purpose(n_intervals: int):
    #     df_conv_by_month = pd.read_sql("""
    #             select  dd.month, count( distinct f.user_id) as "Số lượng user", sum(f.no_conv) as "Số lượng conversation", sum(f.no_silent) as "Số lượng conversation Sani không trả lời"
    #             from fact f join dim_date dd
    #             on f.date = dd.full_date
    #             group by dd.month
    #             order by dd.month asc;
    #         """, conn)
    #
    #     fig = px.bar(df_conv_by_month,
    #                  x="month", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
    #                  title="Số conversation theo tháng", text_auto=True, barmode="group")
    #     fig.update_layout(
    #         legend_title="",
    #         legend=dict(
    #             orientation="h",
    #             yanchor="top",
    #             # xanchor="right",
    #             y=-0.2,
    #             # x=1,
    #         ))
    #
    #     return fig
