# sani_dashboard


## Setup

```
pip3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

## Run

```
python3 app.py
```

## Dashboard
- http://127.0.0.1:8050/