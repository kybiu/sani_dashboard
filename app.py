from maindash import app
from views.layout import make_layout
from callbacks.callbacks_part1 import get_callbacks_part1
from callbacks.callbacks_part2 import get_callbacks_part2
from callbacks.callbacks_part3 import get_callbacks_part3
from callbacks.utility import connect_db

if __name__ == '__main__':
    app.title = "Sani Dashboard"
    app.layout = make_layout()
    db_sqlalchemy = connect_db()
    conn = db_sqlalchemy.connect()
    get_callbacks_part1(app, conn)
    get_callbacks_part2(app, conn)
    get_callbacks_part3(app, conn)
    app.run_server(host='0.0.0.0',debug=True)
