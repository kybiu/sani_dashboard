from dash import html, dcc
from datetime import date
from datetime import datetime, timedelta


def make_layout():
    yesterday = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')
    ten_day_before_yesterday = (datetime.today() - timedelta(days=10)).strftime('%Y-%m-%d')

    return html.Div(children=[
        html.Div(id="dashboard_title", className="nav p-3", children=[
            html.Div(id="title", children=[
                html.H1(children='Sani Dashboard')
            ])
        ]),

        dcc.Interval(
            id='graph-update',
            interval=300000,
            n_intervals=0
        ),
        html.Div(id="graph_container", className="container", children=[
            html.Div(id="part1", className="w-100 d-flex justify-content-between mb-3", children=[
                dcc.Graph(
                    id='resp_rate',
                    className="card s-card-custom"
                ),

                dcc.Graph(
                    id='user_retention_rate',
                    className="card s-card-custom"
                ),

                dcc.Graph(
                    id='success_rate',
                    className="card s-card-custom"
                ),

            ]),

            html.Div(id="part3", className="card s-card-custom w-100 mb-3", children=[
                dcc.DatePickerRange(
                    id='date_picker',
                    display_format='D/M/YYYY',
                    min_date_allowed=date(1995, 8, 5),
                    max_date_allowed=date(2077, 9, 19),
                    initial_visible_month=date.today(),
                    start_date=date(year=int(ten_day_before_yesterday.split("-")[0]),
                                    month=int(ten_day_before_yesterday.split("-")[1]),
                                    day=int(ten_day_before_yesterday.split("-")[2])),
                    end_date=date(year=int(yesterday.split("-")[0]),
                                  month=int(yesterday.split("-")[1]),
                                  day=int(yesterday.split("-")[2])),

                ),

                dcc.Graph(
                    id='conv_by_day',
                ),

                dcc.Graph(
                    id='silent_rate_table',
                ),

            ]),

            html.Div(id="part2", className="card s-card-custom flex-row flex-wrap", children=[
                dcc.Graph(
                    id='conv_by_month',
                    className="w-50"
                ),

                dcc.Graph(
                    id='user_back_by_month',
                    className="w-50"
                ),

                # dcc.Graph(
                #     id='place_and_purpose',
                #     className="w-50"
                # ),
            ]),

        ]),

    ])
