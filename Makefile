version=1.6.2

all: docker_build docker_push

docker_build:
	docker build -f ./docker/Dockerfile -t sanichat/sani-dashboard:$(version) .
	docker build -f ./docker/Dockerfile -t sanichat/sani-dashboard:latest .

docker_push:
	docker push sanichat/sani-dashboard:$(version)
